package lb

import (
	context "context"
	fmt "fmt"
	"testing"
	"time"

	"github.com/golang/protobuf/ptypes"

	lbpb "git.autistici.org/ale/lb/proto"
)

func TestNode_MatchTags(t *testing.T) {
	n := &node{
		status: &lbpb.NodeStatus{
			NodeName: "n1",
			Tags: []*lbpb.Tag{
				&lbpb.Tag{
					Key:   "region",
					Value: "a",
				},
				&lbpb.Tag{
					Key:   "color",
					Value: "blue",
				},
			},
		},
	}

	if ok := n.matchTags(nil); !ok {
		t.Errorf("matchTags(nil) returned false, expected true")
	}
	if ok := n.matchTags([]*lbpb.Tag{
		&lbpb.Tag{
			Key:   "region",
			Value: "a",
		},
	}); !ok {
		t.Errorf("matchTags(region=a) returned false, expected true")
	}
	if ok := n.matchTags([]*lbpb.Tag{
		&lbpb.Tag{
			Key:   "region",
			Value: "b",
		},
	}); ok {
		t.Errorf("matchTags(region=b) returned true, expected false")
	}
}

func makeRequests(lb *Loadbalancer, tags []*lbpb.Tag, n int) map[string]int {
	counters := make(map[string]int)
	for i := 0; i < n; i++ {
		result, err := lb.Select(context.Background(), &lbpb.SelectRequest{Tags: tags})
		if err != nil {
			counters["error"]++
		} else {
			counters[result.NodeName]++
		}
		if result.Overload {
			counters["overload"]++
		}
		if result.Overflow {
			counters["overflow"]++
		}
	}
	return counters
}

func populateNodes(lb *Loadbalancer, numNodes int, reqIncr uint64, cpuUtilFunc func(int) float64, tagsFunc func(int) []*lbpb.Tag) {
	// Run updates twice, to allow for computation of rates, at
	// one second of distance.
	ts := time.Date(2020, 1, 1, 10, 0, 0, 0, time.UTC)
	interval := 1 * time.Second
	for i := 0; i < 2; i++ {
		// nolint: errcheck
		pts, _ := ptypes.TimestampProto(ts)
		ts = ts.Add(interval)

		for n := 0; n < numNodes; n++ {
			var cpuUtil float64
			if cpuUtilFunc != nil {
				cpuUtil = cpuUtilFunc(n)
			}
			var tags []*lbpb.Tag
			if tagsFunc != nil {
				tags = tagsFunc(n)
			}
			// nolint: errcheck
			lb.Update(context.Background(), &lbpb.NodeStatus{
				Timestamp:   pts,
				NodeName:    fmt.Sprintf("n%d", n),
				Ready:       true,
				NumRequests: uint64(i) * reqIncr,
				Utilization: []*lbpb.Utilization{
					&lbpb.Utilization{
						Kind:      lbpb.Utilization_COUNTER,
						Dimension: "qps",
						Value:     float64(i) * float64(reqIncr),
					},
					&lbpb.Utilization{
						Kind:      lbpb.Utilization_GAUGE,
						Dimension: "cpu",
						Value:     cpuUtil,
					},
				},
				Tags: tags,
			})
		}
	}
}

func TestLB_SingleUpdate(t *testing.T) {
	l := New([]*Limit{
		&Limit{
			Dimension: "qps",
			Min:       10,
			Max:       10000,
		},
	}, 1, 10*time.Second)

	populateNodes(l, 1, 100, func(_ int) float64 { return 0.5 }, nil)

	result, err := l.Select(context.Background(), &lbpb.SelectRequest{})
	if err != nil {
		t.Fatalf("Select: %v", err)
	}
	if result.Overload {
		t.Errorf("unexpected overload")
	}
	if result.Overflow {
		t.Errorf("unexpected overflow")
	}
	if result.NodeName != "n0" {
		t.Errorf("unexpected result '%s'", result)
	}

	// Examine internals.
	node := l.nodes["n0"]
	cost := node.cost["qps"]
	if cost != 1 {
		t.Fatalf("bad cost: %v", cost)
	}
}

func TestLB_MinCost(t *testing.T) {
	l := New([]*Limit{
		&Limit{
			Dimension: "qps",
			Min:       10,
			Max:       10000,
		},
		&Limit{
			Dimension: "cpu",
			Max:       1,
		},
	}, 1, 10*time.Second)

	// All counters at zero will test the minCost distribution.
	populateNodes(l, 2, 0, nil, nil)

	counters := makeRequests(l, nil, 100000)
	c0 := counters["n0"]
	c1 := counters["n1"]
	if c0 == 0 || c1 == 0 || c0 != c1 {
		t.Fatalf("unexpected result distribution: %+v", counters)
	}
}

func TestLB_SkewedLoad(t *testing.T) {
	l := New([]*Limit{
		&Limit{
			Dimension: "qps",
			Min:       10,
			Max:       10000,
		},
		&Limit{
			Dimension: "cpu",
			Max:       1,
		},
	}, 1, 10*time.Second)

	// We need to set numRequests and the cpu utilization so that
	// the estimated cost, multiplied by the number of test
	// requests, does not cause overload.
	populateNodes(l, 2, 100, func(n int) float64 {
		if n == 0 {
			return 0.01
		}
		return 1
	}, nil)

	// 1000 requests with the above parameters should not trigger overload.
	counters := makeRequests(l, nil, 1000)
	c0 := counters["n0"]
	c1 := counters["n1"]
	if c0 == 0 || c1 != 0 || c0 != 1000 {
		t.Errorf("unexpected result distribution: %+v", counters)
	}

	if counters["overload"] != 0 {
		t.Errorf("overload was triggered: %+v", counters)
	}

	// 10k requests should trigger overload due to the projected
	// cpu utilization increasing above 1.
	counters = makeRequests(l, nil, 10000)
	if counters["overload"] == 0 {
		t.Errorf("overload was not triggered when expected: %+v", counters)
	}

	// Check the estimated query costs (for the "cpu" dimension).
	cost0 := l.nodes["n0"].cost["cpu"]
	cost1 := l.nodes["n1"].cost["cpu"]
	if cost0 != 0.0001 {
		t.Errorf("unexpected cost for n0/cpu: %v", cost0)
	}
	if cost1 != 0.01 {
		t.Errorf("unexpected cost for n1/cpu: %v", cost1)
	}
}

func TestLB_Region(t *testing.T) {
	l := New([]*Limit{
		&Limit{
			Dimension: "qps",
			Min:       10,
			Max:       10000,
		},
	}, 1, 10*time.Second)

	populateNodes(l, 2, 100, nil, func(n int) []*lbpb.Tag {
		region := "one"
		if n > 0 {
			region = "two"
		}
		return []*lbpb.Tag{
			&lbpb.Tag{
				Key:   "region",
				Value: region,
			},
		}
	})

	counters := makeRequests(l, []*lbpb.Tag{
		&lbpb.Tag{
			Key:   "region",
			Value: "one",
		},
	}, 10000)

	// We should have seen 99 requests overflowing.
	c1 := counters["n1"]
	overflows := counters["overflow"]
	if c1 != 99 {
		t.Errorf("bad number of requests to second node: %v, exp. 99", c1)
	}
	if overflows != c1 {
		t.Errorf("overflowed requests were not marked as such (%d vs 99)", overflows)
	}
}

func TestLB_MandatoryTags(t *testing.T) {
	l := New([]*Limit{
		&Limit{
			Dimension: "qps",
			Min:       10,
			Max:       10000,
		},
	}, 1, 10*time.Second)

	populateNodes(l, 2, 100, nil, func(n int) []*lbpb.Tag {
		region := "one"
		if n > 0 {
			region = "two"
		}
		return []*lbpb.Tag{
			&lbpb.Tag{
				Key:   "region",
				Value: region,
			},
		}
	})

	// A request with a non existing *mandatory* tag should return an error.
	_, err := l.Select(context.Background(), &lbpb.SelectRequest{
		MandatoryTags: []*lbpb.Tag{
			&lbpb.Tag{
				Key:   "region",
				Value: "nonexisting",
			},
		},
	})
	if err == nil {
		t.Fatalf("no error returned")
	}

}

func BenchmarkLB_FewNodes(b *testing.B) {
	l := New([]*Limit{
		&Limit{
			Dimension: "qps",
			Min:       10,
			Max:       10000,
		},
	}, 1, 10*time.Second)

	populateNodes(l, 10, 100, nil, nil)

	b.ResetTimer()

	makeRequests(l, nil, b.N)
}

func BenchmarkLB_ManyNodes(b *testing.B) {
	l := New([]*Limit{
		&Limit{
			Dimension: "qps",
			Min:       10,
			Max:       10000,
		},
	}, 1, 10*time.Second)

	populateNodes(l, 10000, 100, nil, nil)

	b.ResetTimer()

	makeRequests(l, nil, b.N)
}
