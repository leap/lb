package lbagent

import (
	"context"

	lbpb "git.autistici.org/ale/lb/proto"
	"google.golang.org/grpc"
)

type grpcDialer struct {
	conn *grpc.ClientConn
}

func NewGRPCDialer(lbAddr string, grpcOpts ...grpc.DialOption) (Dialer, error) {
	conn, err := grpc.Dial(lbAddr, grpcOpts...)
	if err != nil {
		return nil, err
	}
	return &grpcDialer{conn: conn}, nil
}

func (d *grpcDialer) Close() {
	d.conn.Close()
}

func (d *grpcDialer) Update(ctx context.Context, req *lbpb.NodeStatus) error {
	client := lbpb.NewLoadBalancerClient(d.conn)
	_, err := client.Update(ctx, req)
	return err
}
