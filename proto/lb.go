//go:generate protoc --go_out=plugins=grpc,paths=source_relative:. -I. lb.proto
package lbpb

func (ns *NodeStatus) GetUtilizationByDimension(dim string) float64 {
	for _, u := range ns.Utilization {
		if u.Dimension == dim {
			return u.Value
		}
	}
	return 0
}
