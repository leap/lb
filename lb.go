package lb

import (
	"context"
	"errors"
	"sort"
	"sync"
	"time"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/golang/protobuf/ptypes/timestamp"

	lbpb "git.autistici.org/ale/lb/proto"
)

var minCost = 1e-6

type node struct {
	// A copy of the latest NodeStatus.
	status *lbpb.NodeStatus

	// The current predicted utilization. This is reset to the
	// utilization reported in lbpb.NodeStatus whenever we receive a
	// ping from the node.
	utilization map[string]float64

	// The current estimate of request cost. This is updated on
	// node ping by dividing the utilization delta by the number
	// of requests sent to the node during the update interval.
	cost map[string]float64
}

func newNode() *node {
	return &node{
		utilization: make(map[string]float64),
		cost:        make(map[string]float64),
	}
}

func (n *node) name() string {
	return n.status.NodeName
}

func (n *node) matchTags(tags []*lbpb.Tag) bool {
	// This algorithm is O(N^2), but this way we allow nodes to
	// change tags dynamically, and do not have to maintain the
	// annoying overhead of rebuilding a tags map on every call to
	// update(). We expect the tag set to be very small.
	for _, mtag := range tags {
		ok := false
		for _, tag := range n.status.Tags {
			if tag.Key == mtag.Key {
				ok = (tag.Value == mtag.Value)
				break
			}
		}
		if !ok {
			return false
		}
	}
	return true
}

func (n *node) matchAnyTag(tags []*lbpb.Tag) bool {
	for _, mtag := range tags {
		for _, tag := range n.status.Tags {
			if tag.Key == mtag.Key && tag.Value == mtag.Value {
				return true
			}
		}
	}
	return false
}

func (n *node) matchedTag(tags []*lbpb.Tag) []bool {
	matched := make([]bool, len(tags))
	for i, mtag := range tags {
		matched[i] = false
		for _, tag := range n.status.Tags {
			if tag.Key == mtag.Key && tag.Value == mtag.Value {
				matched[i] = true
				break
			}
		}
	}
	return matched
}

func timestampDiff(a, b *timestamp.Timestamp) time.Duration {
	ta, _ := ptypes.Timestamp(a)
	tb, _ := ptypes.Timestamp(b)
	return ta.Sub(tb)
}

func (n *node) update(status *lbpb.NodeStatus) {
	// Reset utilization to the observed one, eventually computing
	// (istantaneous) rates for counter-type utilization values.
	skipCounters := (n.status == nil)
	var dt float64
	if !skipCounters {
		dt = timestampDiff(status.Timestamp, n.status.Timestamp).Seconds()
		if dt < 0 {
			// Out-of-order update, ignore.
			return
		}
	}
	for _, u := range status.Utilization {
		value := u.Value
		if !skipCounters && u.Kind == lbpb.Utilization_COUNTER {
			prevValue := n.status.GetUtilizationByDimension(u.Dimension)
			if prevValue > u.Value {
				// Counter got reset, skip this update.
				continue
			}
			value = (u.Value - prevValue) / dt
		}
		n.utilization[u.Dimension] = value
	}

	// Make a copy, status might not persist beyond the current
	// request. Keep the old NumRequests value though, so we can
	// compute the requests delta.
	var oldNumRequests uint64
	if n.status != nil {
		oldNumRequests = n.status.NumRequests
	}
	n.status = proto.Clone(status).(*lbpb.NodeStatus)

	// Compute cost along each dimension.
	if status.NumRequests <= oldNumRequests {
		// Counter has been reset (or is unchanged), skip cost
		// updates this cycle.
		return
	}
	reqDelta := float64(status.NumRequests - oldNumRequests)
	for dim, util := range n.utilization {
		n.cost[dim] = util / reqDelta
	}
}

func (n *node) incr() {
	for dim := range n.utilization {
		cost := n.cost[dim]
		if cost < minCost {
			cost = minCost
		}
		n.utilization[dim] += cost
	}
}

// Limit for utilization along a specific dimension. Used to compute
// fullness by mapping raw metrics onto the [min, max] range.
type Limit struct {
	Dimension string
	Min, Max  float64
}

func (l *Limit) fullness(util float64) float64 {
	// Anything below min is 0.
	if util < l.Min {
		return 0
	}
	return (util - l.Min) / (l.Max - l.Min)
}

func (n *node) fullness(limits []*Limit) float64 {
	var f float64
	for _, l := range limits {
		// Utilization defaults to 0 if we haven't seen it yet.
		u := n.utilization[l.Dimension]
		dimf := l.fullness(u)
		if dimf > f {
			f = dimf
		}
	}
	return f
}

// The Loadbalancer collects utilization metrics from nodes and tries
// to make sensible decisions about where to send incoming requests.
//
// Loadbalancer objects are goroutine-safe but that's only because
// they're controlled with a giant mutex.
type Loadbalancer struct {
	limits         []*Limit
	targetFullness float64
	nodeTimeout    time.Duration

	mx    sync.Mutex
	nodes map[string]*node
}

// New returns a new Loadbalancer with the given utilization limits
// and target fullness (1 = 100%). Nodes that we haven't seen for at
// least a nodeTimeout interval are considered dead.
func New(limits []*Limit, targetFullness float64, nodeTimeout time.Duration) *Loadbalancer {
	l := &Loadbalancer{
		limits:         limits,
		targetFullness: targetFullness,
		nodeTimeout:    nodeTimeout,
		nodes:          make(map[string]*node),
	}

	go func() {
		tick := time.NewTicker(1 * time.Second)
		for range tick.C {
			l.expireDeadNodes()
		}
	}()

	return l
}

func (l *Loadbalancer) expireDeadNodes() {
	l.mx.Lock()
	deadline := time.Now().Add(-l.nodeTimeout)
	for name, node := range l.nodes {
		if node.status == nil {
			continue
		}
		ts, _ := ptypes.Timestamp(node.status.Timestamp)
		if ts.Before(deadline) {
			delete(l.nodes, name)
		}
	}
	l.mx.Unlock()
}

func (l *Loadbalancer) getReadyNodes(out []*node) []*node {
	for _, n := range l.nodes {
		if n.status.Ready {
			out = append(out, n)
		}
	}
	return out
}

func (l *Loadbalancer) filterNodesByTag(nodes []*node, tags []*lbpb.Tag) []*node {
	// Reuse the 'nodes' array in-place.
	o := 0
	for i := 0; i < len(nodes); i++ {
		if nodes[i].matchTags(tags) {
			if i != o {
				nodes[o] = nodes[i]
			}
			o++
		}
	}
	return nodes[:o]
}

func (l *Loadbalancer) filterNodesByAnyTag(nodes []*node, tags []*lbpb.Tag) []*node {
	// Reuse the 'nodes' array in-place.
	o := 0
	for i := 0; i < len(nodes); i++ {
		if nodes[i].matchAnyTag(tags) {
			if i != o {
				nodes[o] = nodes[i]
			}
			o++
		}
	}
	return nodes[:o]
}

var errNoNodesAvailable = errors.New("no nodes available")

func (l *Loadbalancer) pickOne(nodes []*node) (*node, float64, error) {
	// Find utilization for each node, pick the smallest.
	var minNode *node
	var minFullness float64
	for i, node := range nodes {
		fullness := node.fullness(l.limits)
		if i == 0 || fullness < minFullness {
			minFullness = fullness
			minNode = node
		}
	}
	if minNode == nil {
		return nil, 0, errNoNodesAvailable
	}
	return minNode, minFullness, nil
}

// Select a node among the available ones, preferably with the given
// tags. Tags should be passed in preference order, as they will be
// progressively dropped starting from the tail should we not find any
// available nodes matching the tag set.
//
// The first return boolean value will be set if the node utilization
// is above 100% along any dimension (the "overload" signal), while
// the second returned boolean will be true if we had to drop tags, so
// it can be used as an "overflow" signal.
//
// Implements the lbpb.LoadBalancer RPC service.
func (l *Loadbalancer) Select(_ context.Context, req *lbpb.SelectRequest) (*lbpb.SelectResponse, error) {
	l.mx.Lock()
	defer l.mx.Unlock()

	for i := 0; i <= len(req.Tags); i++ {
		// Since the following call is the inner core of our
		// algorithm, optimize its memory allocations a bit by
		// using a pool of node slices.
		nodes := getNodeSlice()
		nodes = l.getReadyNodes(nodes)
		if len(req.MandatoryTags) > 0 {
			nodes = l.filterNodesByTag(nodes, req.MandatoryTags)
		}
		nodes = l.filterNodesByTag(nodes, req.Tags[:len(req.Tags)-i])
		node, fullness, err := l.pickOne(nodes)
		putNodeSlice(nodes)
		// Skip this tag aggregation level if fullness > 1
		// unless we are at the last possible ("global") level.
		if i < len(req.Tags) && fullness > l.targetFullness {
			continue
		}
		if err == nil {
			// Increment our approximate view of target
			// utilization.
			node.incr()
			return &lbpb.SelectResponse{
				NodeName: node.name(),
				Overflow: (i > 0),
				Overload: (fullness > l.targetFullness),
				Fullness: fullness,
			}, nil
		}
	}
	return nil, errNoNodesAvailable
}

type sortableNodes struct {
	nodes          []*node
	fullness       map[string]float64
	matchedTags    map[string][]bool
	targetFullness float64
}

func newSortableNodes(nodes []*node, limits []*Limit, tags []*lbpb.Tag, targetFullness float64) *sortableNodes {
	sn := sortableNodes{
		nodes:          nodes,
		fullness:       make(map[string]float64, len(nodes)),
		matchedTags:    make(map[string][]bool, len(nodes)),
		targetFullness: targetFullness,
	}
	for _, n := range nodes {
		sn.fullness[n.name()] = n.fullness(limits)
		sn.matchedTags[n.name()] = n.matchedTag(tags)
	}
	return &sn
}

func (sn sortableNodes) Len() int {
	return len(sn.nodes)
}

func (sn sortableNodes) Less(i, j int) bool {
	nameI := sn.nodes[i].name()
	nameJ := sn.nodes[j].name()

	fullnessI := sn.fullness[nameI]
	fullnessJ := sn.fullness[nameJ]
	if fullnessI > sn.targetFullness && fullnessJ <= sn.targetFullness {
		return false
	}
	if fullnessJ > sn.targetFullness && fullnessI <= sn.targetFullness {
		return true
	}

	for i, tagI := range sn.matchedTags[nameI] {
		if sn.matchedTags[nameJ][i] == tagI {
			continue
		}
		return tagI
	}

	return fullnessI <= fullnessJ
}

func (sn sortableNodes) Swap(i, j int) {
	sn.nodes[i], sn.nodes[j] = sn.nodes[j], sn.nodes[i]
}

func (l *Loadbalancer) ListSelection(_ context.Context, req *lbpb.ListRequest) (*lbpb.ListResponse, error) {
	l.mx.Lock()
	defer l.mx.Unlock()

	// Since the following call is the inner core of our
	// algorithm, optimize its memory allocations a bit by
	// using a pool of node slices.
	nodes := getNodeSlice()
	nodes = l.getReadyNodes(nodes)
	if len(req.Select.MandatoryTags) > 0 {
		nodes = l.filterNodesByTag(nodes, req.Select.MandatoryTags)
	}
	nodes = l.filterNodesByAnyTag(nodes, req.Select.Tags)
	if len(nodes) == 0 {
		return nil, errNoNodesAvailable
	}

	sn := newSortableNodes(nodes, l.limits, req.Select.Tags, l.targetFullness)
	sort.Sort(sn)

	resp := lbpb.ListResponse{
		Nodes: make([]*lbpb.SelectResponse, len(nodes)),
	}
	for i, node := range nodes {
		overflow := false
		for _, tag := range sn.matchedTags[node.name()] {
			if !tag {
				overflow = true
				break
			}
		}
		fullness := node.fullness(l.limits)
		resp.Nodes[i] = &lbpb.SelectResponse{
			NodeName: node.status.NodeName,
			Overflow: overflow,
			Overload: (fullness > l.targetFullness),
			Fullness: fullness,
		}
	}
	nodes[0].incr()
	return &resp, nil
}

// Update our view of the status of a node. Implements the
// lbpb.LoadBalancer RPC service.
func (l *Loadbalancer) Update(_ context.Context, status *lbpb.NodeStatus) (*empty.Empty, error) {
	l.mx.Lock()
	defer l.mx.Unlock()

	node, ok := l.nodes[status.NodeName]
	if !ok {
		node = newNode()
		l.nodes[status.NodeName] = node
	}
	node.update(status)

	return &empty.Empty{}, nil
}

// A pool of slices, to be used by filterReadyNodesByTag() which is
// otherwise allocation-intensive.
var slicePool chan []*node

func init() {
	slicePool = make(chan []*node, 100)
}

func getNodeSlice() []*node {
	select {
	case s := <-slicePool:
		// Reset len, preserve cap.
		return s[:0]
	default:
		return nil
	}
}

func putNodeSlice(s []*node) {
	select {
	case slicePool <- s:
	default:
	}
}
